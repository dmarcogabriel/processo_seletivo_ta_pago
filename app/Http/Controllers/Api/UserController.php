<?php

namespace App\Http\Controllers\Api;

use App\Entities\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\People;
use Symfony\Component\Translation\Dumper\PoFileDumper;

class UserController extends Controller
{
    public function create(Request $req) {
        $req->validate([
            'name' => 'required',
            'cpf' => 'required',
            'bornDate' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);

        $people = new People();

        $people->nome = $req->name;
        $people->cpf = $req->cpf;
        $people->data_nascimento = $req->bornDate;
        $people->telefone = $req->phone;
        $people->email = $req->email;

        $people->save();

        return response([
            'user' => $people,
            'message' => 'Usuário criado com sucesso!'
        ], 200);
    }

    public function get($limit = 5) {
        return People::paginate($limit);
    }

    public function update(Request $req, $id) {
        $req->validate([
            'name' => 'required',
            'cpf' => 'required',
            'bornDate' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);

        $people = People::find($id);

        $people->nome = $req->name;
        $people->cpf = $req->cpf;
        $people->data_nascimento = $req->bornDate;
        $people->telefone = $req->phone;
        $people->email = $req->email;

        $people->save();

        return response([
            'user' => $people,
            'message' => 'Usuário atualizado com sucesso!'
        ], 200);
    }

    public function delete($id) {
        $addresses = People::find($id)->address;

        foreach ($addresses as $address) {
            Address::destroy($address->id);
        }

        People::destroy($id);
        return response('Usuário deletado com sucesso!', 200);
    }

    public function getById($id) {
        $user = People::find($id);

        return ["user" => $user, "addresses" => $user->address];
    }
}
