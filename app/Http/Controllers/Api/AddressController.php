<?php

namespace App\Http\Controllers\Api;

use App\Entities\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function create(Request $req) {
        $req->validate([
            'address' => 'required',
            'addressNumber' => 'required',
            'complement' => 'nullable',
            'district' => 'required',
            'city' => 'required',
            'uf' => 'required',
            'personCode' => 'required',
        ]);

        $address = new Address();

        $address->endereco = $req->address;
        $address->numero_endereco = $req->addressNumber;
        $address->complemento = $req->complement;
        $address->bairro = $req->district;
        $address->cidade = $req->city;
        $address->uf = $req->uf;
        $address->cod_pessoa = $req->personCode;

        $address->save();

         return response('Endereço cadastrado com sucesso!', 200);
    }

    public function get($limit = 5) {
        return Address::paginate($limit);
    }

    public function delete($id) {
        Address::destroy($id);

        return response(['message' => 'Endereço deletado com sucesso!'], 200);
    }

    public function getById($id) {
        $address = Address::find($id);
        return $address;
    }
}
