<?php

namespace App\Http\Controllers;

use App\Entities\People;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        return view('pages/welcome');
    }

    public function register(Request $req) {
        if (count($req->query->all())) {
            $user_id = $req->query->all()["user_id"];
            return view('pages/cadastro')->with(["user_id" => $user_id]);
        }
        return view('pages/cadastro');
    }

    public function register_address(Request $req, $user_id) {
        if (count($req->query->all())) {
            $address_id = $req->query->all()["address_id"];
            return view('pages/register_address')
                ->with(["user_id" => $user_id, "address_id" => $address_id]);
        }
        return view('pages/register_address')
            ->with(['user_id' => $user_id]);
    }

    public function details($id) {
        $user = People::find($id);
        $addresses = $user->address;

        return view('pages/details')
            ->with([
                'user' => $user,
                'addresses' => $addresses
            ]);
    }
}
