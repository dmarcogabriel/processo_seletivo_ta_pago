<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{


    /**
     * Get user of address
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Entities\People', 'cod_pessoa');
    }
}
