<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function address () {
        return $this->hasMany('App\Entities\Address', 'cod_pessoa');
    }
}
