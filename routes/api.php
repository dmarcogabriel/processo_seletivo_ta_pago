<?php

Route::prefix('v1')->group(function () {
    Route::get('/users/{limit?}', 'Api\UserController@get');
    Route::get('/user/{id}', 'Api\UserController@getById');
    Route::post('/users', 'Api\UserController@create');
    Route::delete('/users/{id}', 'Api\UserController@delete');
    Route::post('/users/{id}', 'Api\UserController@update');

    Route::get('/address/{limit?}', 'Api\AddressController@get');
    Route::get('/address-by-id/{id}', 'Api\AddressController@getById');
    Route::post('/address', 'Api\AddressController@create');
    Route::post('address/{id}', 'Api\AddressController@update');
    Route::delete('/address/{id}', 'Api\AddressController@delete');
});
