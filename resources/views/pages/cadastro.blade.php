@extends('components/assets')

@section('content')
    <app-register
        link="{{ @route('home') }}"
        userid="{{ $user_id ?? ''}}"
        logo="{{ @asset('/images/logo.png') }}"
    ></app-register>
@endsection()
