import axios from 'axios';

const baseURL = 'http://localhost:8000/api/v1';
const headers = {
    'Access-Control-Allow-Origin': '*',
};

export const api = () => {
    return axios.create({ baseURL, headers });
};
